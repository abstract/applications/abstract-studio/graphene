using Gtk;

namespace Graphene {
    class HeaderBar : Gtk.HeaderBar {
        public HeaderBar(Gtk.Window window, Gtk.Application app) {
            this.title = "Graphene";
            this.show_close_button = true;
        }
    }
}
