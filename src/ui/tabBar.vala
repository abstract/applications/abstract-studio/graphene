using Gtk;

namespace Graphene {
    class TabBar : Gtk.Box {
        public Gtk.Button menuButton;
        public Gtk.Entry searchBox;

        public TabBar(Gtk.Window window, Gtk.Application app){
            this.get_style_context().add_class("subheader_with_tabs");

            this.set_orientation(Gtk.Orientation.HORIZONTAL);
            this.menuButton = new Gtk.Button.with_label("...");

            this.pack_start(this.menuButton, true, true);

            this.searchBox = new Gtk.Entry();
            this.searchBox.placeholder_text = "Filter";
            this.pack_end(this.searchBox, false, false);

            this.show_all();
        }
    }
}
