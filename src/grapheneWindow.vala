using Gtk;


namespace Graphene {

	class MainWindow : Gtk.Application {
		public Gtk.Window window;
		public Gtk.Box content;
		public Graphene.TabBar tabBar;

        public MainWindow(){

        }

        protected override void activate () {
            this.window = new Gtk.Window();

            this.window.title = "Graphene";
			this.window.set_position (Gtk.WindowPosition.CENTER);
			this.window.set_default_size (800, 600);
			this.window.destroy.connect (Gtk.main_quit);
			this.window.set_titlebar(new Graphene.HeaderBar(this.window, this));

			this.content = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);


			this.tabBar = new Graphene.TabBar(this.window, this);
			this.content.add(tabBar);

			this.content.add(new Gtk.Button.with_label("WTF"));

			this.window.add(this.content);

            this.window.show_all();
            /* Keep GTK In the Loop.... Eh? Eh? */
            Gtk.main();
        }
	}
}
